package edu.westga.cs1302.musiclibrary.viewmodel;

import java.io.File;
import java.io.FileNotFoundException;
import edu.westga.cs1302.musiclibrary.controllers.LibraryController;
import edu.westga.cs1302.musiclibrary.datatier.MusicLibraryFileReader;
import edu.westga.cs1302.musiclibrary.datatier.MusicLibraryFileWriter;
import edu.westga.cs1302.musiclibrary.model.Song;
import edu.westga.cs1302.musiclibrary.resources.UI;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;

/**
 * SongsViewModel mediates between the view and the rest of the program.
 * 
 * @author CS 1301
 * @version Spring 2018
 *
 */
public class SongsViewModel {
	private LibraryController libraryController;

	private StringProperty title;
	private StringProperty artist;
	private DoubleProperty price;
	private IntegerProperty year;
	private IntegerProperty rating;
	
	private ObjectProperty<Song> selectedSong;

	private ListProperty<Song> songs;

	/**
	 * Creates a new MusicLibraryViewModel object with its properties initialized.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param libraryController
	 *            the music library controller
	 * 
	 */
	public SongsViewModel(LibraryController libraryController) {
		if (libraryController == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.LIBRARY_CONTROLLER_CANNOT_BE_NULL);
		}
		
		this.libraryController = libraryController;

		this.title = new SimpleStringProperty();
		this.artist = new SimpleStringProperty();
		this.price = new SimpleDoubleProperty();
		this.year = new SimpleIntegerProperty();
		this.rating = new SimpleIntegerProperty();
		this.selectedSong = new SimpleObjectProperty<Song>();

		this.songs = new SimpleListProperty<Song>(FXCollections.observableArrayList(this.libraryController.getAllSongs()));
	}

	/**
	 * Adds a song that has been entered to the library and the updates the stats
	 * 
	 * @precondition none
	 * @postcondition getLibraryStats() has been updated with new library stats
	 * 
	 * @return true iff song was added and false otherwise
	 */
	public boolean addSong() {
		Song newSong = new Song(this.title.getValue(), this.artist.getValue(), this.year.getValue(), this.price.getValue(), this.rating.getValue()); 
		boolean added = this.libraryController.addSong(newSong, LibraryController.MY_SONGS_ALBUM);
		
		if (added) {
			this.resetFormEntryData();
			this.songs.set(FXCollections.observableArrayList(this.libraryController.getAllSongs()));
		}

		return added;
	}

	private void resetFormEntryData() {
		this.title.set(null);
		this.artist.set(null);
		this.year.set(UI.Text.DEFAULT_YEAR);
		this.price.set(0);
		this.rating.set(1);
	}

	/**
	 * Updates specified song by title and artist with year and price.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return true iff song was updated and false otherwise
	 */
	public boolean updateSong() {
		boolean updated = this.libraryController.updateSong(this.title.getValue(), this.artist.getValue(),
				this.year.getValue(), this.price.getValue(), this.rating.getValue());

		if (!updated) {
			this.resetFormEntryData();
			return false;
		}

		return true;
	}

	/**
	 * Searches for the song that matches the title and artist specified. If a song
	 * is found, then it deletes that song from the library.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return True if it finds a song in the library that matches title and artist
	 *         and it successfully deletes it, false otherwise.
	 */
	public boolean deleteSong() {
		Song songToDelete = this.selectedSong.getValue();
		boolean songDeleted = this.libraryController.deleteSong(songToDelete.getTitle(), songToDelete.getArtist());

		if (songDeleted) {
			this.resetFormEntryData();
			this.songs.set(FXCollections.observableArrayList(this.libraryController.getAllSongs()));
		}

		return songDeleted;
	}

	/**
	 * Gets the song's title
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return The song's title
	 */
	public StringProperty getTitle() {
		return this.title;
	}

	/**
	 * Sets the song's title
	 * 
	 * @precondition none
	 * @postcondition getTitle() == title
	 * 
	 * @param title
	 *            Song's title
	 */
	public void setTitle(StringProperty title) {
		this.title = title;
	}

	/**
	 * Gets the song's artist
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return The song's artist
	 */
	public StringProperty getArtist() {
		return this.artist;
	}

	/**
	 * Sets the song's artist
	 * 
	 * @precondition none
	 * @postcondition getArtist() == artist
	 * 
	 * @param artist
	 *            Song's artist
	 */
	public void setArtist(StringProperty artist) {
		this.artist = artist;
	}

	/**
	 * Gets the song's year
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return The song's year
	 */
	public IntegerProperty getYear() {
		return this.year;
	}

	/**
	 * Sets the song's year
	 * 
	 * @precondition none
	 * @postcondition getYear() == year
	 * 
	 * @param year
	 *            Song's year
	 */
	public void setYear(IntegerProperty year) {
		this.year = year;
	}

	/**
	 * Gets the song's price
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return The song's price
	 */
	public DoubleProperty getPrice() {
		return this.price;
	}

	/**
	 * Sets the song's price
	 * 
	 * @precondition none
	 * @postcondition getPrice() == price
	 * 
	 * @param price
	 *            The price of the song
	 * 
	 */
	public void setPrice(DoubleProperty price) {
		this.price = price;
	}
	
	/**
	 * Gets the song's rating
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return The song's rating
	 */
	public IntegerProperty getRating() {
		return this.rating;
	}

	/**
	 * Sets the song's rating
	 * 
	 * @precondition none
	 * @postcondition getRating() == rating
	 * 
	 * @param rating
	 *            Song's rating
	 */
	public void setRating(IntegerProperty rating) {
		this.rating = rating;
	}

	/**
	 * Gets the selected song.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the selectedSong
	 */
	public ObjectProperty<Song> getSelectedSong() {
		return this.selectedSong;
	}

	/**
	 * Sets the selected song.
	 * 
	 * @precondition none
	 * @postcondition getSelectedSong() == selectedSong
	 *
	 * @param selectedSong
	 *            the selectedSong to set
	 */
	public void setSelectedSong(ObjectProperty<Song> selectedSong) {
		this.selectedSong.set(selectedSong.get());
		this.title.set(selectedSong.get().getTitle());
		this.artist.set(selectedSong.get().getArtist());
		this.year.set(selectedSong.get().getYear());
		this.price.set(selectedSong.get().getPrice());
		this.rating.set(selectedSong.get().getRating());
	}

	/**
	 * Gets the songs.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the songs
	 */
	public ListProperty<Song> getSongs() {
		return this.songs;
	}

	/**
	 * Loads the music library file using the specified File object.
	 * 
	 * @param file
	 *            The file to load the music library from.
	 * 
	 * @throws FileNotFoundException
	 *             If file not found.
	 */
	public void loadLibrary(File file) throws FileNotFoundException {
		MusicLibraryFileReader reader = new MusicLibraryFileReader(file);
	
		//ArrayList<Song> songs = reader.loadAllMySongs();
		//this.libraryController.addSongsToMySongsAlbum(songs);
		
		reader.loadAllMySongs(this.libraryController.getLibrary());
		
		this.resetFormEntryData();
		this.songs.set(FXCollections.observableArrayList(this.libraryController.getAllSongs()));
	}

	/**
	 * Save the music library to the specified File.
	 *
	 * @param file the file to save the music library to
	 * @return true, if save successful
	 */
	public boolean saveLibrary(File file) {
		MusicLibraryFileWriter writer = new MusicLibraryFileWriter(file);
		return writer.write(this.libraryController.getLibrary());
	}


}
