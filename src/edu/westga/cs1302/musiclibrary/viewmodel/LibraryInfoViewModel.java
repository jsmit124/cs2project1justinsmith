package edu.westga.cs1302.musiclibrary.viewmodel;

import edu.westga.cs1302.musiclibrary.controllers.LibraryController;
import edu.westga.cs1302.musiclibrary.model.Album;
import edu.westga.cs1302.musiclibrary.model.Song;
import edu.westga.cs1302.musiclibrary.resources.UI;
import edu.westga.cs1302.musiclibrary.view.format.OutputFormatter;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * ViewModel for the UI that handles displaying the library information.
 * 
 * @author CS 1302
 * @version Spring 2018
 */
public class LibraryInfoViewModel {
	private OutputFormatter formatter;
	private LibraryController libraryController;

	private StringProperty libraryInfo;
	private StringProperty artist;
	private ObjectProperty<Album> selectedAlbum;
	private ObjectProperty<Song> selectedSong;

	/**
	 * Creates the view model
	 * 
	 * @precondition libraryController != null
	 * @postcondition none
	 * @param libraryController
	 *            controller that interacts with the music library
	 */
	public LibraryInfoViewModel(LibraryController libraryController) {
		if (libraryController == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.LIBRARY_CONTROLLER_CANNOT_BE_NULL);
		}

		this.libraryController = libraryController;
		this.formatter = new OutputFormatter();

		this.libraryInfo = new SimpleStringProperty();
		this.selectedAlbum = new SimpleObjectProperty<Album>();
		this.selectedSong = new SimpleObjectProperty<Song>();
		this.artist = new SimpleStringProperty();
	}

	/**
	 * Updates the summary output based on the song selected
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public void displaySongInSummaryOutput() {
		Song song = this.selectedSong.getValue();
		String report = this.formatter.buildSummaryReport(song);
		this.libraryInfo.set(report);
	}
	
	/**
	 * Updates the summary output based on the artist entered
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public void displaySongsByArtist() {
		String artist = this.artist.getValue();
		
		//Album album = this.libraryController.getAlbum(LibraryController.MY_SONGS_ALBUM);
		//String report = album.getAllSongsByArtist(artist);	
		
		String report = this.formatter.buildSummaryReport(artist, this.libraryController.getLibrary());
		this.libraryInfo.set(report);
	}

	/**
	 * Gets information about the library of songs
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return The library of songs and some stats about them
	 */
	public StringProperty getLibraryInfo() {
		return this.libraryInfo;
	}

	/**
	 * Sets the song library display
	 * 
	 * @precondition none
	 * @postcondition getLibraryInfo() == libraryInfo
	 * 
	 * @param libraryInfo
	 *            Music library inventory and stats
	 * 
	 */
	public void setLibraryInfo(StringProperty libraryInfo) {
		this.libraryInfo = libraryInfo;
	}

	/**
	 * Gets the entered artist
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return The entered artist
	 */
	public StringProperty getArtist() {
		return this.artist;
	}

	/**
	 * Sets the artist
	 * 
	 * @precondition none
	 * @postcondition getArtist() == artist
	 * 
	 * @param artist
	 *            artist
	 * 
	 */
	public void setArtist(StringProperty artist) {
		this.artist = artist;
	}

	/**
	 * Gets the selected album.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the selected album
	 */
	public ObjectProperty<Album> getSelectedAlbum() {
		return this.selectedAlbum;
	}

	/**
	 * Sets the selected album.
	 * 
	 * @precondition none
	 * @postcondition getSelectedAlbum() == selectedAlbum
	 *
	 * @param selectedAlbum
	 *            the selectedAlbum to set
	 */
	public void setSelectedAlbum(ObjectProperty<Album> selectedAlbum) {
		this.selectedAlbum = selectedAlbum;
		if (selectedAlbum != null) {
			String output = this.formatter.buildSummaryReport(this.selectedAlbum.get());
			this.libraryInfo.set(output);
		}
	}

	/**
	 * Gets the selected song.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the selected song
	 */
	public ObjectProperty<Song> getSelectedSong() {
		return this.selectedSong;
	}

	/**
	 * Sets the selected album.
	 * 
	 * @precondition none
	 * @postcondition getSelectedSong() == selectedSong
	 *
	 * @param selectedSong
	 *            the selectedSong to set
	 */
	public void setSelectedSong(ObjectProperty<Song> selectedSong) {
		this.selectedSong = selectedSong;
		String output = this.formatter.buildSummaryReport(this.selectedSong.get());
		this.libraryInfo.set(output);
	}

}
