package edu.westga.cs1302.musiclibrary.viewmodel;

import edu.westga.cs1302.musiclibrary.controllers.LibraryController;
import edu.westga.cs1302.musiclibrary.model.Album;
import edu.westga.cs1302.musiclibrary.model.Song;
import edu.westga.cs1302.musiclibrary.resources.UI;
import javafx.beans.property.ListProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;

/**
 * Used to add songs to an album
 * 
 * @author CS1302
 * @version Spring 2018
 */
public class LibraryViewModel {
	private LibraryController libraryController;

	private StringProperty albumName;
	private ListProperty<Album> albums;

	@SuppressWarnings("unused")
	private ListProperty<Song> songs;

	private ObjectProperty<Song> selectedSongToAddToAlbum;
	private ObjectProperty<Album> selectedAlbumToAddSongTo;

	/**
	 * Creates the library view model to interact with the specified library
	 * controller.
	 * 
	 * @precondition libraryController != null
	 * @postcondition none
	 * 
	 * @param libraryController
	 *            the music library controller
	 */
	public LibraryViewModel(LibraryController libraryController) {
		if (libraryController == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.LIBRARY_CONTROLLER_CANNOT_BE_NULL);
		}

		this.libraryController = libraryController;

		this.albumName = new SimpleStringProperty();
		this.songs = new SimpleListProperty<Song>(
				FXCollections.observableArrayList(this.libraryController.getAllSongs()));
		this.albums = new SimpleListProperty<Album>(
				FXCollections.observableArrayList(this.libraryController.getAlbums()));

		this.selectedSongToAddToAlbum = new SimpleObjectProperty<Song>();
		this.selectedAlbumToAddSongTo = new SimpleObjectProperty<Album>();
	}

	/**
	 * Used to create an album, the album name used is from the text field.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return true, if album created, false otherwise
	 */
	public boolean createAlbum() {
		boolean created = this.libraryController.createAlbum(this.albumName.getValue());

		if (created) {
			this.albums.set(FXCollections.observableArrayList(this.libraryController.getAlbums()));
		}

		return created;
	}

	/**
	 * Adds selected song to selected album.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return true, if song added to album false, otherwise
	 */
	public boolean addSongToAlbum() {
		Song selectedSong = this.selectedSongToAddToAlbum.get();
		Album selectedAlbum = this.selectedAlbumToAddSongTo.get();

		return this.libraryController.addSong(selectedSong, selectedAlbum);
	}

	/**
	 * Gets the album's name
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return The album's name
	 */
	public StringProperty getAlbumName() {
		return this.albumName;
	}

	/**
	 * Sets the album's name
	 * 
	 * @precondition none
	 * @postcondition getAlbumName() == albumName
	 * 
	 * @param albumName
	 *            album's name
	 */
	public void setAlbumName(StringProperty albumName) {
		this.albumName = albumName;
	}

	/**
	 * Gets the collection of albums.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the albums
	 */
	public ListProperty<Album> getAlbums() {
		return this.albums;
	}

	/**
	 * Sets the collection of albums.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param albums
	 *            The collection of albums
	 */
	public void setAlbums(ListProperty<Album> albums) {
		this.albums = albums;
	}

	/**
	 * Gets the selected song that would like to add to the selected album
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return The selected song to add to selected album
	 */
	public ObjectProperty<Song> getSelectedSongToAddToAlbum() {
		return this.selectedSongToAddToAlbum;
	}

	/**
	 * Sets the selected song that would like to add to the selected album
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param selectedSongToAddToAlbum
	 *            selected song to add to album
	 */
	public void setSelectedSongToAddToAlbum(ObjectProperty<Song> selectedSongToAddToAlbum) {
		this.selectedSongToAddToAlbum = selectedSongToAddToAlbum;
	}

	/**
	 * Gets the selected album that would like to add to the selected song to
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return The selected album to add selected song to
	 */
	public ObjectProperty<Album> getSelectedAlbumToAddSongTo() {
		return this.selectedAlbumToAddSongTo;
	}

	/**
	 * Sets the selected album that would like to add to selected song to
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param selectedAlbumToAddSongTo
	 *            Selected album to add selected song to
	 * 
	 */
	public void setSelectedAlbumToAddSongTo(ObjectProperty<Album> selectedAlbumToAddSongTo) {
		this.selectedAlbumToAddSongTo = selectedAlbumToAddSongTo;
	}
	
	/**
	 * Refreshes the collection of albums
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public void updateAlbums() {
		this.albums.set(FXCollections.observableArrayList(this.libraryController.getAlbums()));
	}

}
