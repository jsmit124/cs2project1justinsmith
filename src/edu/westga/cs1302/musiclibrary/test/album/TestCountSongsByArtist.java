package edu.westga.cs1302.musiclibrary.test.album;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.musiclibrary.model.Album;
import edu.westga.cs1302.musiclibrary.model.Song;

class TestCountSongsByArtist {

	@Test
	void testArtistNull() {
		Album album = new Album("Test");
		assertThrows(IllegalArgumentException.class, () -> album.countSongsByArtist(null)); 
	}

	@Test
	void testNoSongs() {
		Album album = new Album("Test");
		
		int result = album.countSongsByArtist("Artist 1");
		
		assertEquals(0, result);
	}
	
	@Test
	void testOneSongAndItIsByArtist() {
		Album album = new Album("Test");
		Song song1 = new Song("My title 1", "Artist 1", 2000, 0.99, 3);
		album.add(song1);
		
		int result = album.countSongsByArtist("Artist 1");
		
		assertEquals(1, result);
	}
	
	@Test
	void testOneSongAndNotByArtist() {
	Album album = new Album("Test");
		Song song1 = new Song("My title 1", "Artist 1", 2000, 0.99, 3);
		album.add(song1);
		
		int result = album.countSongsByArtist("Artist A");
		
		assertEquals(0, result);
	}
	
	@Test
	void testMultipleSongsByArtist() {
	Album album = new Album("Test");
		Song song1 = new Song("My title 1", "Artist 1", 2000, 0.99, 3);
		Song song2 = new Song("My title 2", "artist 1", 2000, 0.99, 3);
		Song song3 = new Song("My title 3", "Artist 1", 2000, 0.99, 3);
		album.add(song1);
		album.add(song2);
		album.add(song3);
		
		int result = album.countSongsByArtist("Artist 1");
		
		assertEquals(3, result);
	}
	
	@Test
	void testMultipleSongsButNotAllByArtist() {
	Album album = new Album("Test");
		Song song1 = new Song("My title 1", "Artist 1", 2000, 0.99, 3);
		Song song2 = new Song("My title 2", "Artist 2", 2000, 0.99, 3);
		Song song3 = new Song("My title 3", "Artist 1", 2000, 0.99, 3);
		album.add(song1);
		album.add(song2);
		album.add(song3);
		
		int result = album.countSongsByArtist("Artist 1");
		
		assertEquals(2, result);
	}
	
	

}
