package edu.westga.cs1302.musiclibrary.test.album;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.musiclibrary.model.Album;
import edu.westga.cs1302.musiclibrary.model.Song;

class TestFindYearOfOldestSong {

	@Test
	void testNoSongs() {
		Album theAlbum = new Album("Test");
		
		int result = theAlbum.findYearOfOldestSong();
		
		assertEquals(Integer.MAX_VALUE, result);
	}
	
	@Test
	void testOneSong() {
		Album theAlbum = new Album("Test");
		Song song = new Song("My title 1", "My artist", 2000, 0.99, 3);
		theAlbum.add(song);
				
		int result = theAlbum.findYearOfOldestSong();
		
		assertEquals(2000, result);
	}

	@Test
	void testThreeSongsOldestFirst() {
		Album theAlbum = new Album("Test");
		Song song1 = new Song("My title 1", "My artist", 2000, 0.99, 3);
		Song song2 = new Song("My title 2", "My artist", 2001, 0.99, 3);
		Song song3 = new Song("My title 3", "My artist", 2002, 0.99, 3);
		theAlbum.add(song1);
		theAlbum.add(song2);
		theAlbum.add(song3);
				
		int result = theAlbum.findYearOfOldestSong();
		
		assertEquals(2000, result);
	}
	
	@Test
	void testThreeSongsOldestMiddle() {
		Album theAlbum = new Album("Test");
		Song song1 = new Song("My title 1", "My artist", 2000, 0.99, 3);
		Song song2 = new Song("My title 2", "My artist", 2001, 0.99, 3);
		Song song3 = new Song("My title 3", "My artist", 2002, 0.99, 3);
		theAlbum.add(song2);
		theAlbum.add(song1);
		theAlbum.add(song3);
				
		int result = theAlbum.findYearOfOldestSong();
		
		assertEquals(2000, result);
	}
	
	@Test
	void testThreeSongsOldestLast() {
		Album theAlbum = new Album("Test");
		Song song1 = new Song("My title 1", "My artist", 2000, 0.99, 3);
		Song song2 = new Song("My title 2", "My artist", 2001, 0.99, 3);
		Song song3 = new Song("My title 3", "My artist", 2002, 0.99, 3);
		theAlbum.add(song2);
		theAlbum.add(song3);
		theAlbum.add(song1);
				
		int result = theAlbum.findYearOfOldestSong();
		
		assertEquals(2000, result);
	}

}
