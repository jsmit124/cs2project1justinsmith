package edu.westga.cs1302.musiclibrary.test.album;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.musiclibrary.model.Album;
import edu.westga.cs1302.musiclibrary.model.Song;

class TestFindNameOfMainArtist {

	@Test
	void testNoSongs() {
		Album theAlbum = new Album("Test");
		
		String result = theAlbum.findNameOfMainArtist();
		
		assertEquals("", result);
	}
	
	@Test
	void testOneSong() {
		Album theAlbum = new Album("Test");
		Song song = new Song("My title 1", "Artist 1", 2000, 0.99, 3);
		theAlbum.add(song);
		
		String result = theAlbum.findNameOfMainArtist();
		
		assertEquals("Artist 1", result);
	}

	@Test
	void testMultipleSongsAllSameArtist() {
		Album theAlbum = new Album("Test");
		Song song1 = new Song("My title 1", "Artist 1", 2000, 0.99, 3);
		Song song2 = new Song("My title 2", "artist 1", 2001, 0.99, 3);
		Song song3 = new Song("My title 3", "Artist 1", 2002, 0.99, 3);
		theAlbum.add(song1);
		theAlbum.add(song2);
		theAlbum.add(song3);
		
		String result = theAlbum.findNameOfMainArtist();
		
		assertEquals("Artist 1", result);
	}
	
	@Test
	void testMultipleSongsCaseInsensitiveTest() {
		Album theAlbum = new Album("Test");
		Song song1 = new Song("My title 1", "Artist 1", 2000, 0.99, 3);
		Song song2 = new Song("My title 2", "artist 2", 2001, 0.99, 3);
		Song song3 = new Song("My title 3", "Artist 2", 2002, 0.99, 3);
		theAlbum.add(song1);
		theAlbum.add(song2);
		theAlbum.add(song3);
		
		String result = theAlbum.findNameOfMainArtist();
		
		assertEquals("artist 2", result);
	}
	
	
	@Test
	void testMultipleSongsAndMultipleArtistsNoneWithSameNumberOfSongs() {
		Album theAlbum = new Album("Test");
		Song song1 = new Song("My title 1", "Artist 1", 2000, 0.99, 3);
		Song song2 = new Song("My title 2", "Artist 2", 2001, 0.99, 3);
		Song song3 = new Song("My title 3", "Artist 1", 2002, 0.99, 3);
		Song song4 = new Song("My title 4", "Artist 2", 2002, 0.99, 3);
		Song song5 = new Song("My title 5", "Artist 2", 2002, 0.99, 3);
		Song song6 = new Song("My title 6", "Artist 3", 2002, 0.99, 3);
		theAlbum.add(song1);
		theAlbum.add(song2);
		theAlbum.add(song3);
		theAlbum.add(song4);
		theAlbum.add(song5);
		theAlbum.add(song6);
		
		String result = theAlbum.findNameOfMainArtist();
		
		assertEquals("Artist 2", result);
	}
	
	@Test
	void testMultipleSongsMultipleArtistsWithSameNumberOfSongs() {
		Album theAlbum = new Album("Test");
		Song song1 = new Song("My title 1", "Artist 1", 2000, 0.99, 3);
		Song song2 = new Song("My title 2", "Artist 2", 2001, 0.99, 3);
		Song song3 = new Song("My title 3", "Artist 2", 2002, 0.99, 3);
		Song song4 = new Song("My title 4", "Artist 3", 2002, 0.99, 3);
		Song song5 = new Song("My title 5", "Artist 3", 2002, 0.99, 3);
		Song song6 = new Song("My title 6", "Artist 4", 2002, 0.99, 3);
		theAlbum.add(song1);
		theAlbum.add(song2);
		theAlbum.add(song3);
		theAlbum.add(song4);
		theAlbum.add(song5);
		theAlbum.add(song6);
		
		String result = theAlbum.findNameOfMainArtist();
		
		assertEquals("Artist 2", result);
	}

}
