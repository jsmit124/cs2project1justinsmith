package edu.westga.cs1302.musiclibrary.test.album;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.musiclibrary.model.Album;
import edu.westga.cs1302.musiclibrary.model.Song;

class TestDeleteSong {

	@Test
	void testNullTitle() {
		Album theAlbum = new Album("Test");
		
		Song song1 = new Song("My title 1", "Artist 1", 2000, 0.99, 3);
		theAlbum.add(song1);
		
		assertThrows(IllegalArgumentException.class, () -> theAlbum.deleteSong(null, "Artist 1")); 
	}

	@Test
	void testNullArtist() {
		Album theAlbum = new Album("Test");
		
		Song song1 = new Song("My title 1", "Artist 1", 2000, 0.99, 3);
		theAlbum.add(song1);
		
		assertThrows(IllegalArgumentException.class, () -> theAlbum.deleteSong("My title 1", null)); 
	}
	

	@Test
	void testNoSongs() {
		Album theAlbum = new Album("Test");
		
		boolean result = theAlbum.deleteSong("My title 1", "Artist 1");
		assertFalse(result);
	}
	
	@Test
	void testOneSongAndFound() {
		Album theAlbum = new Album("Test");
		
		Song song1 = new Song("My title 1", "Artist 1", 2000, 0.99, 3);
		theAlbum.add(song1);
		
		boolean result = theAlbum.deleteSong("My title 1", "Artist 1");
		assertTrue(result);
		assertEquals(0, theAlbum.size());
	}
	
	@Test
	void testOneSongAndFoundWithNonMatchingCase() {
		Album theAlbum = new Album("Test");
		
		Song song1 = new Song("My title 1", "Artist 1", 2000, 0.99, 3);
		theAlbum.add(song1);
		
		boolean result = theAlbum.deleteSong("my TITLE 1", "artIST 1");
		assertTrue(result);
		assertEquals(0, theAlbum.size());
	}
	
	@Test
	void testOneSongAndNotFoundDueToTitle() {
		Album theAlbum = new Album("Test");
		
		Song song1 = new Song("My title 1", "Artist 1", 2000, 0.99, 3);
		theAlbum.add(song1);
		
		boolean result = theAlbum.deleteSong("My title 2", "Artist 1");
		assertFalse(result);
		assertEquals(1, theAlbum.size());
	}
	
	@Test
	void testOneSongAndNotFoundDueToArtist() {
		Album theAlbum = new Album("Test");
		
		Song song1 = new Song("My title 1", "Artist 1", 2000, 0.99, 3);
		theAlbum.add(song1);
		
		boolean result = theAlbum.deleteSong("My title 1", "Artist 2");
		assertFalse(result);
		assertEquals(1, theAlbum.size());
	}
	
	@Test
	void testFoundMultipleSongsAndSameArtist() {
		Album theAlbum = new Album("Test");
		
		Song song1 = new Song("My title 1", "Artist 1", 2000, 0.99, 3);
		Song song2 = new Song("My title 2", "Artist 1", 2001, 0.99, 3);
		Song song3 = new Song("My title 3", "Artist 1", 2002, 0.99, 3);
		theAlbum.add(song1);
		theAlbum.add(song2);
		theAlbum.add(song3);

		boolean result = theAlbum.deleteSong("My title 3", "Artist 1");
		
		assertTrue(result);
		assertEquals(2, theAlbum.size());
	}

	@Test
	void testFoundMultipleSongsWithSameTitleAndDifferentArtist() {
		Album theAlbum = new Album("Test");
		
		Song song1 = new Song("My title 1", "Artist 1", 2000, 0.99, 3);
		Song song2 = new Song("My title 1", "Artist 2", 2001, 0.99, 3);
		Song song3 = new Song("My title 1", "Artist 3", 2002, 0.99, 3);
		theAlbum.add(song1);
		theAlbum.add(song2);
		theAlbum.add(song3);

		boolean result = theAlbum.deleteSong("My title 1", "Artist 3");
		
		assertTrue(result);
		assertEquals(2, theAlbum.size());
	}
	
	@Test
	void testFoundMultipleSongsAndArtistsFoundFirst() {
		Album theAlbum = new Album("Test");
		
		Song song1 = new Song("My title 1", "Artist 1", 2000, 0.99, 3);
		Song song2 = new Song("My title 2", "Artist 2", 2001, 0.99, 3);
		Song song3 = new Song("My title 3", "Artist 3", 2002, 0.99, 3);
		theAlbum.add(song1);
		theAlbum.add(song2);
		theAlbum.add(song3);

		boolean result = theAlbum.deleteSong("My title 1", "Artist 1");
		
		assertTrue(result);
		assertEquals(2, theAlbum.size());
	}

	@Test
	void testFoundMultipleSongsAndArtistsFoundMiddle() {
		Album theAlbum = new Album("Test");
		
		Song song1 = new Song("My title 1", "Artist 1", 2000, 0.99, 3);
		Song song2 = new Song("My title 2", "Artist 2", 2001, 0.99, 3);
		Song song3 = new Song("My title 3", "Artist 3", 2002, 0.99, 3);
		theAlbum.add(song2);
		theAlbum.add(song1);
		theAlbum.add(song3);

		boolean result = theAlbum.deleteSong("My title 1", "Artist 1");
		
		assertTrue(result);
		assertEquals(2, theAlbum.size());
	}

	@Test
	void testFoundMultipleSongsAndArtistsFoundLast() {
		Album theAlbum = new Album("Test");
		
		Song song1 = new Song("My title 1", "Artist 1", 2000, 0.99, 3);
		Song song2 = new Song("My title 2", "Artist 2", 2001, 0.99, 3);
		Song song3 = new Song("My title 3", "Artist 3", 2002, 0.99, 3);
		theAlbum.add(song2);
		theAlbum.add(song3);
		theAlbum.add(song1);
		
		boolean result = theAlbum.deleteSong("My title 1", "Artist 1");
		
		assertTrue(result);
		assertEquals(2, theAlbum.size());
	}
	
}
