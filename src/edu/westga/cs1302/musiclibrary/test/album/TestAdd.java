package edu.westga.cs1302.musiclibrary.test.album;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.musiclibrary.model.Album;
import edu.westga.cs1302.musiclibrary.model.Song;

class TestAdd {

	@Test
	void testAddNullSong() {
		Album theAlbum = new Album("Test");
		assertThrows(IllegalArgumentException.class, () -> theAlbum.add(null)); 
	}
	
	@Test
	void testAddSongToEmptyAlbum() {
		Album theAlbum = new Album("Test");
		Song song = new Song("My title", "My artist", 2000, 0.99, 3);
		
		theAlbum.add(song);
		
		assertEquals(1, theAlbum.size()); 
	}
	
	@Test
	void testAddSongToAlbumWithSongs() {
		Album theAlbum = new Album("Test");
		Song song1 = new Song("My title 1", "My artist 2", 2000, 0.99, 3);
		Song song2 = new Song("My title 2", "My artist 2", 2001, 1.99, 3);
		
		theAlbum.add(song1);
		theAlbum.add(song2);
		
		assertEquals(2, theAlbum.size()); 
	}
	
	@Test
	void testAddDuplicateSongToAlbumWithSongs() {
		Album theAlbum = new Album("Test");
		Song song1 = new Song("My title 1", "My artist 2", 2000, 0.99, 3);
		Song song2 = new Song("My title 2", "My artist 2", 2001, 1.99, 3);
		Song song3 = new Song("My title 2", "My artist 2", 2002, 2.99, 3);
		
		
		theAlbum.add(song1);
		theAlbum.add(song2);
		theAlbum.add(song3);
		
		assertEquals(2, theAlbum.size()); 
	}

}
