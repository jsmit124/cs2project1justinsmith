package edu.westga.cs1302.musiclibrary.test.album;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.musiclibrary.model.Album;

class TestConstruction {

	@Test
	void testDefaultConstructor() {
		Album theAlbum = new Album();

		assertAll(() -> assertEquals(0, theAlbum.size()), 
				 () -> assertNull(theAlbum.getName()));
	}
	
	@Test
	void testOneParameterConstructorWithNullName() {
		assertThrows(IllegalArgumentException.class, () -> new Album(null));
	}
	
	@Test
	void testOneParameterConstructorWithEmptyName() {
		assertThrows(IllegalArgumentException.class, () -> new Album(""));
	}
	
	@Test
	void testOneParameterValidConstruction() {
		Album theAlbum = new Album("Programmer's rock");

		assertAll(() -> assertEquals(0, theAlbum.size()), 
				 () -> assertEquals("Programmer's rock", theAlbum.getName()));
	}
	
}
