package edu.westga.cs1302.musiclibrary.test.song;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.musiclibrary.model.Song;

class TestConstruction {

	@Test
	void testValidConstruction() {
		Song aSong = new Song("My title", "My artist", 2015, 0.99, 3);

		assertAll(() -> assertEquals("My title", aSong.getTitle()), 
				 () -> assertEquals("My artist", aSong.getArtist()),
				 () -> assertEquals(2015, aSong.getYear()), 
				 () -> assertEquals(0.99, aSong.getPrice()));
	}
	
	@Test
	void testNullTitle() {
		assertThrows(IllegalArgumentException.class, () -> new Song(null, "Test artist", 2015, 0.99, 3));
	}

	@Test
	void testEmptyTitle() {
		assertThrows(IllegalArgumentException.class, () -> new Song("", "Test artist", 2015, 0.99, 3));
	}

	@Test
	void testNullArtist() {
		assertThrows(IllegalArgumentException.class, () -> new Song("Test title", null, 2015, 0.99, 3));
	}

	@Test
	void testEmptyArtist() {
		assertThrows(IllegalArgumentException.class, () -> new Song("Test title", "", 2015, 0.99, 3));
	}

	@Test
	void testYearBelowCutoff() {
		assertThrows(IllegalArgumentException.class, () -> new Song("Test title", "Test artist", 1875, 0.99, 3));
	}

	@Test
	void testYearAtCutoff() {
		Song aSong = new Song("My title", "My artist", 1876, 0.99, 3);

		assertAll(() -> assertEquals("My title", aSong.getTitle()), 
				 () -> assertEquals("My artist", aSong.getArtist()),
				 () -> assertEquals(1876, aSong.getYear()), 
				 () -> assertEquals(0.99, aSong.getPrice()),
				 () -> assertEquals(3, aSong.getRating()));
	}

	@Test
	void testPriceBelow0() {
		assertThrows(IllegalArgumentException.class, () -> new Song("Test title", "Test artist", 2015, -0.01, 3));
	}

	@Test
	void testPriceAt0() {
		Song aSong = new Song("My title", "My artist", 2015, 0, 3);
		
		assertEquals(0, aSong.getPrice());
	}
	
	@Test
	void testRatingBelow1() {
		assertThrows(IllegalArgumentException.class, () -> new Song("Test title", "Test artist", 2015, 0.99, 0));
	}
	
	@Test
	void testRatingAbove5() {
		assertThrows(IllegalArgumentException.class, () -> new Song("Test title", "Test artist", 2015, 0.99, 6));
	}
	
	@Test
	void testRatingAt1() {
		Song aSong = new Song("My title", "My artist", 2015, 0.99, 1);
		
		assertEquals(1, aSong.getRating());
	}
	
	@Test
	void testRatingAt5() {
		Song aSong = new Song("My title", "My artist", 2015, 0.99, 5);
		
		assertEquals(5, aSong.getRating());
	}

}
