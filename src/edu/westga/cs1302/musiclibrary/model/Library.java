package edu.westga.cs1302.musiclibrary.model;

import java.util.ArrayList;

import edu.westga.cs1302.musiclibrary.resources.UI;

/**
 * Holds a music library which will be a collection of albums.
 * 
 * @author CS1302
 * @version Spring 2018
 */
public class Library {

	private ArrayList<Album> albums;

	/**
	 * Constructs the library which holds a collection of albums.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public Library() {
		this.albums = new ArrayList<Album>();
	}

	/**
	 * Adds the album to the library.
	 * 
	 * @precondition album != null
	 * @postcondition size() == size()@prev + 1
	 * 
	 * @param album
	 *            The album to add.
	 * @return true, if album was successfully added to the library
	 */
	public boolean addAlbum(Album album) {
		if (album == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.ALBUM_CANNOT_BE_NULL);
		}
		for (Album currAlbum : this.albums) {
			if (currAlbum.getName().equalsIgnoreCase(album.getName())) {
				return false;
			}
		}

		return this.albums.add(album);
	}

	/**
	 * Numbers of albums in the music library.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return Numbers of albums in the music library.
	 */
	public int size() {
		return this.albums.size();
	}

	/**
	 * Adds specified song to the specified album
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param song
	 *            Song to add to the album
	 * @param albumName
	 *            Album name to add the album to
	 * @return true if song successfully, added false otherwise
	 */
	public boolean addSong(Song song, String albumName) {
		Album album = this.getAlbum(albumName);
		if (album == null) {
			throw new IllegalArgumentException("album is null");
		}

		for (Song currSong : album.getSongs()) {
			if (currSong.getTitle().equalsIgnoreCase(song.getTitle()) && currSong.getArtist().equalsIgnoreCase(song.getArtist())) {
				return false;
			}
		}
		return album.add(song);

	}

	/**
	 * Adds specified song to the specified album
	 * 
	 * @precondition album != null
	 * @postcondition none
	 * 
	 * @param song
	 *            Song to add to the album
	 * @param album
	 *            Album to add the album to
	 *            
	 * @return true if song successfully, added false otherwise
	 */
	public boolean addSong(Song song, Album album) {
		if (album == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.ALBUM_CANNOT_BE_NULL);
		}

		return album.add(song);
	}

	/**
	 * Gets the album of the specified album name.
	 * 
	 * @precondition albumName != null
	 * @postcondition none
	 * 
	 * @param albumName
	 *            name of the album to get
	 * @return album or null if not found.
	 */
	public Album getAlbum(String albumName) {
		if (albumName == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.ALBUMNAME_CANNOT_BE_NULL);
		}

		for (Album currAlbum : this.albums) {
			if (currAlbum.getName().equalsIgnoreCase(albumName)) {
				return currAlbum;
			}
		}

		return null;
	}

	/**
	 * Gets all the songs on specified album
	 * 
	 * @precondition albumName != null
	 * @postcondition none
	 * 
	 * @param albumName
	 *            name of the album to get
	 * @return all songs on specified album, or null if album not found
	 */
	public ArrayList<Song> getSongsOnAlbum(String albumName) {
		if (albumName == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.ALBUMNAME_CANNOT_BE_NULL);
		}

		Album album = this.getAlbum(albumName);
		if (album != null) {
			return album.getSongs();
		}

		return null;
	}

	/**
	 * Gets the collection of albums in the library
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the collection of albums in the library
	 */
	public ArrayList<Album> getAlbums() {
		return this.albums;
	}

}
