package edu.westga.cs1302.musiclibrary.model;

import edu.westga.cs1302.musiclibrary.resources.UI;

/**
 * The Class Song.
 * 
 * @author CS 1302
 * @version Spring 2018
 */
public class Song {
	public static final int LOWER_BOUND_YEAR = 1876;
	public static final int MIN_RATING = 1;
	public static final int MAX_RATING = 5;
	
	private String title;
	private String artist;
	private int year;
	private double price;
	private int rating;

	/**
	 * Instantiates a new song.
	 *
	 * @precondition title != null && title not empty; artist != null && artist not
	 *               empty; year >= LOWER_BOUND_YEAR (1876); price >= 0; MIN_RATING <= rating <= MAX_RATING; 
	 * @postcondition getTitle() == title; getArtist() == artist; getPrice() ==
	 *                price; getYear() == year; getRating() == rating
	 * 
	 * @param title
	 *            the title
	 * @param artist
	 *            the artist
	 * @param year
	 *            the year
	 * @param price
	 *            the price
	 * @param rating
	 * 			  the rating
	 */
	public Song(String title, String artist, int year, double price, int rating) {
		if (title == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.TITLE_CANNOT_BE_NULL);
		}

		if (title.isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.TITLE_CANNOT_BE_EMPTY);
		}

		if (artist == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.ARTIST_CANNOT_BE_NULL);
		}

		if (artist.isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.ARTIST_CANNOT_BE_EMPTY);
		}

		if (year < LOWER_BOUND_YEAR) {
			throw new IllegalArgumentException(UI.ExceptionMessages.INVALID_YEAR);
		}

		if (price < 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.INVALID_PRICE);
		}
		if (rating < MIN_RATING || rating > MAX_RATING) {
			throw new IllegalArgumentException(UI.ExceptionMessages.INVALID_RATING);
		}
		String formattedTitle = title.replaceAll("\\s+", " ");
		this.title = formattedTitle;
		String formattedArtist = artist.replaceAll("\\s+", " ");
		this.artist = formattedArtist;
		this.year = year;
		this.price = price;
		this.rating = rating;
	}

	/**
	 * Gets the price.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the price
	 */
	public double getPrice() {
		return this.price;
	}

	/**
	 * Sets the price.
	 *
	 * @precondition price >= 0
	 * @precondition getPrice() == price
	 * @param price
	 *            the price to set
	 */
	public void setPrice(double price) {
		if (price < 0) {
			throw new IllegalArgumentException(UI.ExceptionMessages.INVALID_PRICE);
		}

		this.price = price;
	}

	/**
	 * Gets the year.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the year
	 */
	public int getYear() {
		return this.year;
	}

	/**
	 * Sets the year.
	 *
	 * @precondition year >= LOWER_BOUND_YEAR (1876)
	 * @precondition getYear() == year
	 * 
	 * @param year
	 *            the year to set
	 */
	public void setYear(int year) {
		if (year < LOWER_BOUND_YEAR) {
			throw new IllegalArgumentException(UI.ExceptionMessages.INVALID_YEAR);
		}

		this.year = year;
	}

	/**
	 * Gets the title.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the title
	 */
	public String getTitle() {
		return this.title;
	}

	/**
	 * Gets the artist.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the artist
	 */
	public String getArtist() {
		return this.artist;
	}

	/**
	 * Gets the rating
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the rating
	 */
	public int getRating() {
		return this.rating;
	}

	/**
	 * Sets the rating
	 * 
	 * @precondition MIN_RATING <= rating <= MAX_RATING
	 * @param rating the rating to set
	 */
	public void setRating(int rating) {
		if (rating < MIN_RATING || rating > MAX_RATING) {
			throw new IllegalArgumentException(UI.ExceptionMessages.INVALID_RATING);
		}
		this.rating = rating;
	}

	@Override
	public String toString() {
		return this.getTitle() + " by " + this.getArtist();
	}

}
