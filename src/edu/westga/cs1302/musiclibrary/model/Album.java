package edu.westga.cs1302.musiclibrary.model;

import java.util.ArrayList;

import edu.westga.cs1302.musiclibrary.resources.UI;

/**
 * The Class Album.
 * 
 * @author CS 1302
 * @version Spring 2018
 */
public class Album {
	private ArrayList<Song> songs;
	private String name;

	/**
	 * Instantiates a new album.
	 * 
	 * @precondition none
	 * @postcondition size() == 0 AND getName() == null
	 */
	public Album() {
		this.songs = new ArrayList<Song>();
		this.name = null;
	}

	/**
	 * Instantiates a new album.
	 *
	 * @precondition name != null && name is not empty
	 * @postcondition size() == 0 and getName() == name
	 * 
	 * @param name
	 *            the name
	 */
	public Album(String name) {
		if (name == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NAME_CANNOT_BE_NULL);
		}

		if (name.isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NAME_CANNOT_BE_EMPTY);
		}

		this.songs = new ArrayList<Song>();
		
		String formattedName = name.replaceAll("\\s+", " ");
		this.name = formattedName;
	}

	/**
	 * Adds the song to the album.
	 * 
	 * @precondition song != null
	 * @postcondition size() == size()@prev + 1
	 * 
	 * @param song
	 *            The song to add
	 * 
	 * @return true, if successful
	 */
	public boolean add(Song song) {
		if (song == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.SONG_CANNOT_BE_NULL);
		}
		for (Song currSong : this.songs) {
			if (currSong.getTitle().equalsIgnoreCase(song.getTitle()) && currSong.getArtist().equalsIgnoreCase(song.getArtist())) {
				return false;
			}
		}
		return this.songs.add(song);
	}

	/**
	 * Find year of oldest song.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the year of the oldest song or Integer.MAX_VALUE if no songs on
	 *         Album.
	 */
	public int findYearOfOldestSong() {
		int oldestYear = Integer.MAX_VALUE;

		for (Song currSong : this.songs) {
			if (currSong.getYear() < oldestYear) {
				oldestYear = currSong.getYear();
			}
		}

		return oldestYear;
	}

	/**
	 * Find year of newest song.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the year of the newest song or Integer.MIN_VALUE if no songs on
	 *         Album.
	 */
	public int findYearOfNewestSong() {
		int newestYear = Integer.MIN_VALUE;

		for (Song currSong : this.songs) {
			if (currSong.getYear() > newestYear) {
				newestYear = currSong.getYear();
			}
		}

		return newestYear;
	}

	/**
	 * creates a histogram of all songs with price less than 5 in the album
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return a histogram of all songs with price less than 5 in the album
	 */
	public String createPriceHistogram() {
		String asterik = "*";
		String output = System.lineSeparator();
		String lessThan1 = "Songs < $1: ";
		String between1and2 = "Songs < $2: ";
		String between2and3 = "Songs < $3: ";
		String between3and4 = "Songs < $4: ";
		String between4and5 = "Songs < $5: ";
		for (Song currSong : this.songs) {
			if (currSong.getPrice() < 1) {
				lessThan1 += asterik;
			} else if (currSong.getPrice() < 2 && currSong.getPrice() >= 1) {
				between1and2 += asterik;
			} else if (currSong.getPrice() < 3 && currSong.getPrice() >= 2) {
				between2and3 += asterik;
			} else if (currSong.getPrice() < 4 && currSong.getPrice() >= 3) {
				between3and4 += asterik;
			} else if (currSong.getPrice() < 5 && currSong.getPrice() >= 4) {
				between4and5 += asterik;
			}
		}
		output += lessThan1 + System.lineSeparator()
					+ between1and2 + System.lineSeparator()
					+ between2and3 + System.lineSeparator()
					+ between3and4 + System.lineSeparator()
					+ between4and5;
		return output;
	}
	/**
	 * Find name of main artist - the artist with the most songs on the album. If
	 * multiple artists have the same number of most songs, then the first artist on
	 * the album with that song count is returned.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return The name of the artist with most songs on album, "" if no songs on
	 *         album.
	 */
	public String findNameOfMainArtist() {
		String mainArtist = "";
		int maxSongs = 0;

		for (Song currSong : this.songs) {
			String currArtist = currSong.getArtist();
			int songCount = this.countSongsByArtist(currArtist);

			if (songCount > maxSongs) {
				maxSongs = songCount;
				mainArtist = currArtist;
			}
		}

		return mainArtist;
	}

	/**
	 * Count songs by artist.
	 * 
	 * @precondition artist != null
	 * @postcondition none
	 *
	 * @param artist
	 *            the artist
	 * @return the number of songs by the artist
	 */
	public int countSongsByArtist(String artist) {
		if (artist == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.ARTIST_CANNOT_BE_NULL);
		}

		int count = 0;

		for (Song currSong : this.songs) {
			String currentArtist = currSong.getArtist();
			if (currentArtist.equalsIgnoreCase(artist)) {
				count++;
			}
		}

		return count;
	}

	/**
	 * Deletes the specified song from the album. The song to delete (Hint: remove)
	 * must match (case insensitive) the title and artist.
	 * 
	 * @precondition title != null && artist != null
	 * @postcondition if found, size() == size()@prev - 1
	 * 
	 * @param title
	 *            Song's title
	 * @param artist
	 *            Song's artist
	 * @return true if the song was found and deleted from the album, false
	 *         otherwise
	 */
	public boolean deleteSong(String title, String artist) {
		if (title == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.TITLE_CANNOT_BE_NULL);
		}

		if (artist == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.ARTIST_CANNOT_BE_NULL);
		}

		for (Song currSong : this.songs) {
			if (currSong.getTitle().equalsIgnoreCase(title) && currSong.getArtist().equalsIgnoreCase(artist)) {
				return this.songs.remove(currSong);
			}
		}

		return false;
	}

	/**
	 * Searches for the specified title and artist (case insensitive in the album).
	 * If found, then update the found song object with said year and title.
	 * 
	 * @precondition title != null && artist != null
	 * @postcondition if found, found song updated with new year and price
	 * 
	 * @param title
	 *            Song's title to search by
	 * @param artist
	 *            Song's artist to search by
	 * @param year
	 *            Updated song year
	 * @param price
	 *            Updated song price
	 * @param rating
	 * 			  Updating rating
	 * 
	 * @return Returns true if found and updated, false otherwise.
	 */
	public boolean updateSong(String title, String artist, int year, double price, int rating) {
		if (title == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.TITLE_CANNOT_BE_NULL);
		}

		if (artist == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.ARTIST_CANNOT_BE_NULL);
		}

		Song song = this.findSong(title, artist);

		if (song != null) {
			song.setYear(year);
			song.setPrice(price);
			song.setRating(rating);
			return true;
		}

		return false;
	}

	private Song findSong(String title, String artist) {
		for (Song currSong : this.songs) {
			if (currSong.getTitle().equalsIgnoreCase(title) && currSong.getArtist().equalsIgnoreCase(artist)) {
				return currSong;
			}
		}

		return null;
	}

	/**
	 * Gets the size of the album
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return The size of the album
	 */
	public int size() {
		return this.songs.size();
	}

	/**
	 * Gets the name of the album.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * Sets the name of the album.
	 * 
	 * @precondition name != null && name is not empty
	 * @postcondition getName() == name
	 *
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		if (name == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NAME_CANNOT_BE_NULL);
		}

		if (name.isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.NAME_CANNOT_BE_EMPTY);
		}

		this.name = name;
	}

	/**
	 * Gets all the songs on the album.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the songs
	 */
	public ArrayList<Song> getSongs() {
		return this.songs;
	}
	
	/**
	 * Gets all the songs on the album by specified artist
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param artist
	 * 			the artist to find songs
	 * 
	 * @return output
	 * 			all songs by specified artist
	 */
	public String getAllSongsByArtist(String artist) {
		if (artist == null || artist.isEmpty()) {
			throw new IllegalArgumentException("invalid artist");
		}
		String on = " on ";
		String output = "Songs by " + artist + on + this.name + ":" + System.lineSeparator();
		int counter = 0;
		
		for (Song currSong : this.getSongs()) {
			if (currSong.getArtist().equalsIgnoreCase(artist)) {
				output += currSong.getTitle() + System.lineSeparator();
				counter++;
			}
		}
		
		if (counter == 0) {
			output = "No songs by " + artist + on + this.getName() + ".";
		}
		
		return output;
	}

	@Override
	public String toString() {
		return this.getName();
	}

}
