package edu.westga.cs1302.musiclibrary.view;

import java.io.File;
import java.io.FileNotFoundException;

import edu.westga.cs1302.musiclibrary.controllers.LibraryController;
import edu.westga.cs1302.musiclibrary.model.Album;
import edu.westga.cs1302.musiclibrary.model.Song;
import edu.westga.cs1302.musiclibrary.resources.UI;
import edu.westga.cs1302.musiclibrary.view.converter.ValidPositiveWholeNumberStringConverter;
import edu.westga.cs1302.musiclibrary.view.converter.ValidTwoDecimalPositiveNumberStringConverter;
import edu.westga.cs1302.musiclibrary.viewmodel.LibraryInfoViewModel;
import edu.westga.cs1302.musiclibrary.viewmodel.LibraryViewModel;
import edu.westga.cs1302.musiclibrary.viewmodel.SongsViewModel;
import javafx.beans.binding.Bindings;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.FileChooser.ExtensionFilter;
import javafx.stage.Window;

/**
 * MusicLibraryGuiCodeBehind defines the "controller" for MusicLibraryGui.fxml.
 * 
 * @author CS 1302
 * @version Spring 2018
 */
public class MusicLibraryGuiCodeBehind {
	private SongsViewModel songsViewModel;
	private LibraryViewModel libraryViewModel;
	private LibraryInfoViewModel libraryInfoViewModel;

	private ObjectProperty<Song> selectedSong;
	private IntegerProperty selectedRating;

	private ObjectProperty<Song> selectedSongToAddToAlbum;
	private ObjectProperty<Album> selectedAlbumToAddTo;
	private ObjectProperty<Album> selectedAlbumToDisplay;
	private ObjectProperty<Song> selectedSongToDisplay;

	@FXML
	private AnchorPane pane;

	@FXML
	private MenuBar menu;

	@FXML
	private MenuItem openMenuItem;

	@FXML
	private MenuItem saveMenuItem;

	@FXML
	private TextField titleTextField;

	@FXML
	private TextField artistTextField;

	@FXML
	private TextField priceTextField;

	@FXML
	private TextField yearTextField;

	@FXML
	private TextField analyzeArtistTextField;

	@FXML
	private ComboBox<Integer> ratingComboBox;

	@FXML
	private ComboBox<Song> songsComboBox;

	@FXML
	private ComboBox<Album> albumsComboBox;

	@FXML
	private ComboBox<Album> displayAlbumComboBox;

	@FXML
	private ComboBox<Song> displaySongComboBox;

	@FXML
	private TextField albumNameTextField;

	@FXML
	private TextArea libraryInfoTextArea;

	@FXML
	private Button addButton;

	@FXML
	private Button updateButton;

	@FXML
	private Button deleteButton;

	@FXML
	private Button createAlbumButton;

	@FXML
	private Button addToAlbumButton;

	@FXML
	private Button analyzeArtistButton;

	@FXML
	private ListView<Song> songsListView;

	/**
	 * Creates a new MusicLibraryGuiCodeBehind object.
	 * 
	 * @precondition none
	 * @postcondition none
	 */
	public MusicLibraryGuiCodeBehind() {
		LibraryController libraryController = new LibraryController();
		this.songsViewModel = new SongsViewModel(libraryController);
		this.libraryViewModel = new LibraryViewModel(libraryController);
		this.libraryInfoViewModel = new LibraryInfoViewModel(libraryController);

		this.selectedSong = new SimpleObjectProperty<Song>();
		this.selectedRating = new SimpleIntegerProperty();

		this.selectedSongToAddToAlbum = new SimpleObjectProperty<Song>();
		this.selectedAlbumToAddTo = new SimpleObjectProperty<Album>();
		this.selectedAlbumToDisplay = new SimpleObjectProperty<Album>();
		this.selectedSongToDisplay = new SimpleObjectProperty<Song>();
	}

	/**
	 * Initializes the GUI components, binding them to the view model properties and
	 * setting their event handlers.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 */
	@FXML
	public void initialize() {
		this.bindComponentsToViewModel();
		this.setEventActions();

		this.initializeComponents();
	}

	private void setRatingValues() {
		for (int i = Song.MIN_RATING; i <= Song.MAX_RATING; i++) {
			this.ratingComboBox.getItems().add(i);
		}

		this.ratingComboBox.setValue(this.ratingComboBox.getItems().get(0));
	}

	private void initializeComponents() {
		this.selectedSong.setValue(null);
		this.libraryInfoTextArea.setEditable(false);
		this.setRatingValues();

		this.resetSongEntryFields();
		this.updateButtonStatus();

		this.createBindingToEnableAnalyzeArtistButton();
		this.createBindingToEnableCreateAlbumButton();
	}

	private void createBindingToEnableAnalyzeArtistButton() {
		this.analyzeArtistTextField.setText("");
		this.analyzeArtistButton.disableProperty()
				.bind(Bindings.createBooleanBinding(() -> this.analyzeArtistTextField.getText().trim().isEmpty(),
						this.analyzeArtistTextField.textProperty()));
	}

	private void createBindingToEnableCreateAlbumButton() {
		this.albumNameTextField.setText("");
		this.createAlbumButton.disableProperty().bind(Bindings.createBooleanBinding(
				() -> this.albumNameTextField.getText().trim().isEmpty(), this.albumNameTextField.textProperty()));
	}

	private void resetSongEntryFields() {
		this.titleTextField.setText("");
		this.artistTextField.setText("");
		this.yearTextField.setText(Integer.toString(UI.Text.DEFAULT_YEAR));
		this.priceTextField.setText(UI.Text.DEFAULT_PRICE_TEXT);
		this.ratingComboBox.setValue(this.ratingComboBox.getItems().get(0));
	}

	private void bindComponentsToViewModel() {
		this.titleTextField.textProperty().bindBidirectional(this.songsViewModel.getTitle());
		this.artistTextField.textProperty().bindBidirectional(this.songsViewModel.getArtist());
		this.yearTextField.textProperty().bindBidirectional(this.songsViewModel.getYear(),
				new ValidPositiveWholeNumberStringConverter());
		this.priceTextField.textProperty().bindBidirectional(this.songsViewModel.getPrice(),
				new ValidTwoDecimalPositiveNumberStringConverter());

		this.songsListView.itemsProperty().bind(this.songsViewModel.getSongs());
		this.selectedSong.bindBidirectional(this.songsViewModel.getSelectedSong());
		this.selectedRating.bindBidirectional(this.songsViewModel.getRating());

		this.albumNameTextField.textProperty().bindBidirectional(this.libraryViewModel.getAlbumName());

		this.songsComboBox.itemsProperty().bind(this.songsViewModel.getSongs());
		this.albumsComboBox.itemsProperty().bind(this.libraryViewModel.getAlbums());
		this.selectedSongToAddToAlbum.bindBidirectional(this.libraryViewModel.getSelectedSongToAddToAlbum());
		this.selectedAlbumToAddTo.bindBidirectional(this.libraryViewModel.getSelectedAlbumToAddSongTo());

		this.libraryInfoTextArea.textProperty().bindBidirectional(this.libraryInfoViewModel.getLibraryInfo());
		this.displayAlbumComboBox.itemsProperty().bind(this.libraryViewModel.getAlbums());
		this.displaySongComboBox.itemsProperty().bind(this.songsViewModel.getSongs());

		this.selectedAlbumToDisplay.bindBidirectional(this.libraryInfoViewModel.getSelectedAlbum());
		this.selectedSongToDisplay.bindBidirectional(this.libraryInfoViewModel.getSelectedSong());
		this.analyzeArtistTextField.textProperty().bindBidirectional(this.libraryInfoViewModel.getArtist());

		this.setupListenersForSongEntryComponents();
		this.setupListenersForAddSongToAlbumEntryComponents();
		this.setupListenersForDisplayingSummaryInformationEntryComponents();
	}

	private void setupListenersForSongEntryComponents() {
		this.setupListenerForSongsListView();

		this.yearTextField.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,4}")) {
					MusicLibraryGuiCodeBehind.this.yearTextField.setText(oldValue);
				}
			}
		});

		this.priceTextField.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d{0,7}([\\.]\\d{0,2})?")) {
					MusicLibraryGuiCodeBehind.this.priceTextField.setText(oldValue);
				}
			}
		});

		this.ratingComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Integer>() {
			@Override
			public void changed(ObservableValue<? extends Integer> observable, Integer oldValue, Integer newValue) {
				MusicLibraryGuiCodeBehind.this.selectedRating.set(newValue);
			}
		});

	}

	private void setupListenerForSongsListView() {
		this.songsListView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Song>() {
			@Override
			public void changed(ObservableValue<? extends Song> observable, Song oldValue, Song newValue) {
				if (newValue != null) {
					MusicLibraryGuiCodeBehind.this.selectedSong.set(newValue);
					MusicLibraryGuiCodeBehind.this.songsViewModel
							.setSelectedSong(MusicLibraryGuiCodeBehind.this.selectedSong);
					MusicLibraryGuiCodeBehind.this.ratingComboBox.setValue(newValue.getRating());
				}

				MusicLibraryGuiCodeBehind.this.updateButtonStatus();
			}
		});
	}

	private void setupListenersForAddSongToAlbumEntryComponents() {
		this.songsComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Song>() {
			@Override
			public void changed(ObservableValue<? extends Song> observable, Song oldValue, Song newValue) {
				MusicLibraryGuiCodeBehind.this.selectedSongToAddToAlbum.set(newValue);
				MusicLibraryGuiCodeBehind.this.updateAddToAlbumButtonStatus();
			}
		});

		this.albumsComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Album>() {
			@Override
			public void changed(ObservableValue<? extends Album> observable, Album oldValue, Album newValue) {
				MusicLibraryGuiCodeBehind.this.selectedAlbumToAddTo.set(newValue);
				MusicLibraryGuiCodeBehind.this.updateAddToAlbumButtonStatus();
			}
		});

	}

	private void setupListenersForDisplayingSummaryInformationEntryComponents() {
		this.displayAlbumComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Album>() {
			@Override
			public void changed(ObservableValue<? extends Album> observable, Album oldValue, Album newValue) {
				if (newValue != null) {
					MusicLibraryGuiCodeBehind.this.displaySongComboBox.getSelectionModel().clearSelection();
					MusicLibraryGuiCodeBehind.this.selectedAlbumToDisplay.set(newValue);
					MusicLibraryGuiCodeBehind.this.libraryInfoViewModel
							.setSelectedAlbum(MusicLibraryGuiCodeBehind.this.selectedAlbumToDisplay);
				}
			}
		});

		this.displaySongComboBox.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Song>() {
			@Override
			public void changed(ObservableValue<? extends Song> observable, Song oldValue, Song newValue) {
				if (newValue != null) {
					MusicLibraryGuiCodeBehind.this.displayAlbumComboBox.getSelectionModel().clearSelection();
					MusicLibraryGuiCodeBehind.this.selectedSongToDisplay.set(newValue);
					MusicLibraryGuiCodeBehind.this.libraryInfoViewModel
							.setSelectedSong(MusicLibraryGuiCodeBehind.this.selectedSongToDisplay);
					MusicLibraryGuiCodeBehind.this.libraryInfoViewModel.displaySongInSummaryOutput();
				}
			}
		});

	}

	private void showAlert(String title, String message) {
		Alert alert = new Alert(AlertType.INFORMATION);
		Window owner = this.pane.getScene().getWindow();
		alert.initOwner(owner);
		alert.setTitle(title);
		alert.setContentText(message);
		alert.showAndWait();
	}

	private void setEventActions() {
		this.addButton.setOnAction(event -> this.handleAddSongAction());
		this.updateButton.setOnAction(event -> this.handleUpdateSongAction());
		this.deleteButton.setOnAction(event -> this.handleDeleteSongAction());
		this.openMenuItem.setOnAction(event -> this.handleOpenFileAction());
		this.saveMenuItem.setOnAction(event -> this.handleSaveFileAction());
		this.createAlbumButton.setOnAction(event -> this.handleCreateAlbumAction());
		this.addToAlbumButton.setOnAction(event -> this.handleAddSongToAlbumAction());
		this.analyzeArtistButton.setOnAction(event -> this.handleAnalyzeArtistAction());
	}

	private void handleAnalyzeArtistAction() {
		this.displayAlbumComboBox.getSelectionModel().clearSelection();
		this.displaySongComboBox.getSelectionModel().clearSelection();
		this.libraryInfoViewModel.displaySongsByArtist();
	}

	private void handleAddSongToAlbumAction() {
		if (this.albumsComboBox.getSelectionModel().getSelectedIndex() == 0) {
			this.showAlert(UI.Text.INVALID_ADDITION, UI.Text.AUTOMATICALLY_APART);
		} else {
			this.libraryViewModel.addSongToAlbum();
		}
	}

	private void handleCreateAlbumAction() {
		this.libraryViewModel.createAlbum();
		this.updateAddToAlbumButtonStatus();
	}

	private void handleOpenFileAction() {
		FileChooser fileChooser = new FileChooser();
		this.setFileFilters(fileChooser);

		Window owner = this.pane.getScene().getWindow();
		File selectedFile = fileChooser.showOpenDialog(owner);

		if (selectedFile != null) {
			try {
				this.songsViewModel.loadLibrary(selectedFile);
				this.libraryViewModel.updateAlbums();
			} catch (FileNotFoundException e) {
				this.showAlert(UI.Text.FILE_OPEN, e.getMessage());
			}
		}
	}

	private void setFileFilters(FileChooser fileChooser) {
		ExtensionFilter filter = new ExtensionFilter("Music Library Files", "*.mlf");
		fileChooser.getExtensionFilters().add(filter);
		filter = new ExtensionFilter("All Files", "*.*");
		fileChooser.getExtensionFilters().add(filter);
	}

	private void handleSaveFileAction() {
		FileChooser fileChooser = new FileChooser();
		this.setFileFilters(fileChooser);

		Window owner = this.pane.getScene().getWindow();
		File selectedFile = fileChooser.showSaveDialog(owner);

		if (selectedFile != null) {
			boolean saved = this.songsViewModel.saveLibrary(selectedFile);

			if (!saved) {
				this.showAlert(UI.Text.FILE_SAVE, UI.Text.FILE_SAVE_ERROR);
			}
		}

	}
	
	private void clearLibrarySummaryInfoTextAndSelections() {
		this.libraryInfoTextArea.setText("");
		this.analyzeArtistTextField.setText("");
		this.displayAlbumComboBox.getSelectionModel().clearSelection();
		this.displaySongComboBox.getSelectionModel().clearSelection();
	}
	
	private void handleAddSongAction() {
		try {
			if (!this.songsViewModel.addSong()) {
				this.showAlert(UI.Text.FAILURE, UI.Text.DUPLICATE_SONG);
			} else {
				this.updateUIAfterAdd();
			}
		} catch (IllegalArgumentException e) {
			this.showAlert(UI.Text.SONG_ADDITION, e.getMessage());
		}
	}

	private void updateUIAfterAdd() {
		this.resetSongEntryFields();
		this.songsListView.getSelectionModel().clearSelection();
		this.updateButtonStatus();
		this.clearLibrarySummaryInfoTextAndSelections();
	}

	private void handleUpdateSongAction() {
		try {
			boolean updated = this.songsViewModel.updateSong();

			if (updated) {
				this.songsListView.getSelectionModel().clearSelection();
				this.resetSongEntryFields();
				this.clearLibrarySummaryInfoTextAndSelections();
			} else {
				this.showAlert(UI.Text.FAILURE, UI.Text.SONG_UPDATE_FAILED);
			}
		} catch (IllegalArgumentException e) {
			this.showAlert(UI.Text.SONG_UPDATE, e.getMessage());
		}
	}

	private void handleDeleteSongAction() {
		boolean deleted = this.songsViewModel.deleteSong();

		if (deleted) {
			this.songsListView.getSelectionModel().clearSelection();
			this.resetSongEntryFields();
			this.updateButtonStatus();
			this.clearLibrarySummaryInfoTextAndSelections();
		} else {
			this.showAlert(UI.Text.FAILURE, UI.Text.SONG_DELETION_FAILED);
		}

	}

	private void updateButtonStatus() {
		this.updateSongDetailButtonStatus();
		this.updateAddToAlbumButtonStatus();
	}

	private void updateSongDetailButtonStatus() {
		Song songSelected = this.selectedSong.getValue();

		if (this.songsViewModel.getSongs().size() == 0 || songSelected == null) {
			this.deleteButton.setDisable(true);
			this.updateButton.setDisable(true);
		} else {
			this.deleteButton.setDisable(false);
			this.updateButton.setDisable(false);
		}
	}

	private void updateAddToAlbumButtonStatus() {
		if (this.songsComboBox.getSelectionModel().getSelectedIndex() >= 0
				&& this.albumsComboBox.getSelectionModel().getSelectedIndex() >= 1) {
			this.addToAlbumButton.setDisable(false);
		} else {
			this.addToAlbumButton.setDisable(true);
		}
	}

}
