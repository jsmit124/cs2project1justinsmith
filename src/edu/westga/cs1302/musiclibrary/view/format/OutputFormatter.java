package edu.westga.cs1302.musiclibrary.view.format;

import edu.westga.cs1302.musiclibrary.model.Album;
import edu.westga.cs1302.musiclibrary.model.Library;
import edu.westga.cs1302.musiclibrary.model.Song;
import edu.westga.cs1302.musiclibrary.resources.UI;

/**
 * Formats output to be displayed in the UI.
 * 
 * @author CS1302
 * @version Spring 2018
 */
public class OutputFormatter {

	/**
	 * Takes an Album object and builds and formats a summary report about the
	 * album.
	 * 
	 * @precondition album != null
	 * @postcondition none
	 * 
	 * @param album
	 *            The album
	 *            
	 * @return Formatted summary of the album.
	 */
	public String buildSummaryReport(Album album) {
		if (album == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.ALBUM_CANNOT_BE_NULL);
		}
		
		String output;
		output = "Album: " + album.getName() + System.lineSeparator();
		output += "#Songs " + album.size() + System.lineSeparator();
		output += System.lineSeparator();

		output += "Main artist: " + album.findNameOfMainArtist() + System.lineSeparator();
		output += "Year of oldest song: " + album.findYearOfOldestSong() + System.lineSeparator();
		output += "Year of newest song: " + album.findYearOfNewestSong() + System.lineSeparator();
		
		output += album.createPriceHistogram();

		return output;
	}

	/**
	 * Takes the song object and builds and formats a summary report about the
	 * song.
	 * 
	 * @precondition song != null
	 * @postcondition none
	 * 
	 * @param song
	 *            The song
	 *            
	 * @return Formatted summary of the album.
	 */
	public String buildSummaryReport(Song song) {
		if (song == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.SONG_CANNOT_BE_NULL);
		}
		String output;
		output = song.getTitle() + " by " + song.getArtist() + System.lineSeparator();
		output += song.getYear() + System.lineSeparator();
		output += "$" + song.getPrice() + System.lineSeparator();
		
		String starLine = "";
		for (int i = 0; i < song.getRating(); i++) {
			starLine += "*";
		}
		output += "Rating: " + starLine;
		
		return output;
	}
	
	/**
	 * Takes the artist and library objects and builds and formats a summary report about the
	 * songs by artist within library.
	 * 
	 * @precondition library != null && artist != null || empty
	 * @postcondition none
	 * 
	 * @param artist
	 *            The artist
	 * @param library
	 *            The library
	 *            
	 * @return Formatted summary of the artist.
	 */
	public String buildSummaryReport(String artist, Library library) {
		if (artist == null || artist.isEmpty()) {
			throw new IllegalArgumentException("invalid artist");
		}
		if (library == null) {
			throw new IllegalArgumentException("invalid library");
		}
		String output = "Songs by " + artist + ":" + System.lineSeparator();
		int allSongCounter = 0;
		
		for (Album currAlbum : library.getAlbums()) {
			if (currAlbum.getName().equalsIgnoreCase("MY SONGS")) {
				continue;
			}
			int albumSongCounter = 0;
			String albumOutput = "";
			for (Song currSong : currAlbum.getSongs()) {
				if (currSong.getArtist().equalsIgnoreCase(artist)) {
					albumOutput += currSong.getTitle() + System.lineSeparator();
					albumSongCounter++;
					allSongCounter++;
				}
			}
			if (albumSongCounter != 0) {
				albumOutput = currAlbum.getName() + " (" + albumSongCounter + ")" + System.lineSeparator() + albumOutput + System.lineSeparator();
			}
			output += albumOutput;
		}
		if (allSongCounter == 0) {
			output = "No songs by " + artist + " on any albums.";
		}
		return output;
	}
	

}
