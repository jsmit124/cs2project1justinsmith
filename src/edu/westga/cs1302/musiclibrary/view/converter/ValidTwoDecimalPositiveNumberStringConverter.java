package edu.westga.cs1302.musiclibrary.view.converter;

import java.text.DecimalFormat;

import javafx.util.converter.NumberStringConverter;

/**
 * The Class ValidWholeNumberStringConverter.
 * 
 * @author CS1302
 * @version Spring 2018
 */
public class ValidTwoDecimalPositiveNumberStringConverter extends NumberStringConverter {

	/**
	 * Removes all non-digits from the string and then converts it to a number.
	 * 
	 * @param input
	 *            The string to convert
	 */
	@Override
	public Number fromString(String input) {
		input = input.replaceAll("[^\\d|.]", "");

		return super.fromString(input);
	}

	/**
	 * Converts number to string with two decimal digits.
	 * 
	 * @param value
	 *            The number to convert
	 */
	@Override
	public String toString(Number value) {
		DecimalFormat df = new DecimalFormat("#.00");
		String output = df.format(value);

		return output;
	}

}
