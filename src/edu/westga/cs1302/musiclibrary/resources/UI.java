package edu.westga.cs1302.musiclibrary.resources;

/**
 * The Class UIStrings defines output strings that are displayed for the user to
 * see.
 * 
 * @author CS 1302
 * @version Spring 2018
 */
public final class UI {

	/**
	 * Defines string messages for exception messages for music library application.
	 */
	public static final class ExceptionMessages {
		public static final String INVALID_YEAR = "Year must be 1876 or newer.";
		public static final String INVALID_PRICE = "Price must be >= 0.";
		public static final String INVALID_RATING = "Rating must be between 1 and 5, inclusive.";

		public static final String ALBUM_CANNOT_BE_NULL = "Album cannot be null.";

		public static final String SONG_CANNOT_BE_NULL = "Song cannot be null.";
		public static final String SONG_CANNOT_BE_EMPTY = "Song cannot be empty.";

		public static final String TITLE_CANNOT_BE_NULL = "Title cannot be null.";
		public static final String TITLE_CANNOT_BE_EMPTY = "Title cannot be empty.";

		public static final String ARTIST_CANNOT_BE_NULL = "Artist cannot be null.";
		public static final String ARTIST_CANNOT_BE_EMPTY = "Artist cannot be empty.";

		public static final String NAME_CANNOT_BE_NULL = "Name cannot be null.";
		public static final String NAME_CANNOT_BE_EMPTY = "Name cannot be empty.";

		public static final String ALBUMNAME_CANNOT_BE_NULL = "Album name cannot be null.";
		public static final String ALBUMNAME_CANNOT_BE_EMPTY = "Album name cannot be empty.";
		
		public static final String ALBUM_FILE_CANNOT_BE_NULL = "Album file to load is null.";
		public static final String SONGS_CANNOT_BE_NULL = "Songs cannot be null.";
		
		public static final String LIBRARY_CONTROLLER_CANNOT_BE_NULL = "libraryController cannot be null.";
		public static final String LIBRARY_CANNOT_BE_NULL = "library cannot be null.";
				
	}

	/**
	 * Defines string messages to be used in UI messages.
	 */
	public static final class Text {
		public static final String FILE_OPEN = "File open";
		public static final String FILE_SAVE = "File save";
		
		public static final String SONG_DELETION_FAILED = "Song deletion failed.";
		
		public static final String FAILURE = "Failure";
		public static final String SONG_ADDITION = "Add song";
		public static final String SONG_UPDATE = "Update song";
		public static final String DUPLICATE_SONG = "Song already exists";
		public static final String SONG_UPDATE_FAILED = "Song update failed.";
		public static final String INVALID_ADDITION = "Invalid addition";
		public static final String AUTOMATICALLY_APART = "Every song is automatically a part of the MY SONGS album.";
		
		public static final String TITLE_ARTIST_NOT_FOUND = "The specified title and artist was not found.";
		public static final String ARTIST_NOT_FOUND = "The specified artist was not found.";
		public static final String FILE_SAVE_ERROR = "Error saving file. See error log.";
		
		public static final String NO_ARTIST = "No artist";
		public static final String NO_ARTIST_TO_ANALYZE = "No artist entered to analyze.";
		
		public static final int DEFAULT_YEAR = 2000;
		
		public static final String DEFAULT_PRICE_TEXT = "0.00";
	}

}
