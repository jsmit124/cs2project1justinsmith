package edu.westga.cs1302.musiclibrary.datatier;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import edu.westga.cs1302.musiclibrary.model.Album;
import edu.westga.cs1302.musiclibrary.model.Library;
import edu.westga.cs1302.musiclibrary.model.Song;
import edu.westga.cs1302.musiclibrary.resources.UI;

/**
 * The Class AlbumFileWriter.
 * 
 * @author CS1302
 * @version Spring 2018
 */
public class MusicLibraryFileWriter {

	private File albumFile;

	/**
	 * Instantiates a new album file write.
	 *
	 * @precondition albumFile != null
	 * @postcondition none
	 * 
	 * @param albumFile
	 *            the album file
	 */
	public MusicLibraryFileWriter(File albumFile) {
		if (albumFile == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.ALBUM_FILE_CANNOT_BE_NULL);
		}

		this.albumFile = albumFile;
	}

	/**
	 * Writes all the songs in the album in the set album file. Each song will be on
	 * a separate line and of the following format: Album
	 * name,Title,Artist,Year,Price
	 * 
	 * @precondition album != null
	 * 
	 * @param album
	 *            The album (collection of songs) to write to file.
	 * @return true if album successfully, written false otherwise
	 */
	public boolean write(Album album) {
		if (album == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.ALBUM_CANNOT_BE_NULL);
		}

		try (PrintWriter writer = new PrintWriter(this.albumFile)) {
			for (Song currSong : album.getSongs()) {
				String output = this.buildSongPartOfOutputLine(currSong);
				output = album.getName() + MusicLibraryFileReader.FIELD_SEPARATOR + output;
				writer.println(output);
			}
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
			return false;
		}

		return true;
	}
	
	/**
	 * Writes all the songs in the library in the set album file. Each song will be on
	 * a separate line and of the following format: Album
	 * name,Title,Artist,Year,Price
	 * 
	 * @precondition library != null
	 * 
	 * @param library
	 *            The library (collection of albums) to write to file.
	 * @return true if library successfully, written false otherwise
	 */
	public boolean write(Library library) {
		if (library == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.LIBRARY_CANNOT_BE_NULL);
		}
		try (PrintWriter writer = new PrintWriter(this.albumFile)) {
			for (Album currAlbum : library.getAlbums()) {
				if (currAlbum.getName().equalsIgnoreCase("MY SONGS")) {
					continue;
				}
				for (Song currSong : currAlbum.getSongs()) {
					String output = this.buildSongPartOfOutputLine(currSong);
					output = currAlbum.getName() + MusicLibraryFileReader.FIELD_SEPARATOR + output;
					writer.println(output);
				}
			}
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
			return false;
		}
		return true;
	}

	private String buildSongPartOfOutputLine(Song song) {
		String output = song.getTitle() + MusicLibraryFileReader.FIELD_SEPARATOR;
		output += song.getArtist() + MusicLibraryFileReader.FIELD_SEPARATOR;
		output += song.getYear() + MusicLibraryFileReader.FIELD_SEPARATOR;
		output += song.getPrice() + MusicLibraryFileReader.FIELD_SEPARATOR;
		output += song.getRating();

		return output;
	}

}
