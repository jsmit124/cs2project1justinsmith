package edu.westga.cs1302.musiclibrary.datatier;

import java.io.File;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import edu.westga.cs1302.musiclibrary.controllers.LibraryController;
import edu.westga.cs1302.musiclibrary.model.Album;
import edu.westga.cs1302.musiclibrary.model.Library;
import edu.westga.cs1302.musiclibrary.model.Song;
import edu.westga.cs1302.musiclibrary.resources.UI;

/**
 * The Class AlbumFileReader. Reads a .mlf (Music Library File) file which is a
 * CSV file with the following format:
 * 
 * Album name,Title,Artist,Year,Price
 * 
 * @author CS1301
 * @version Spring 2018
 */
public class MusicLibraryFileReader {
	public static final String FIELD_SEPARATOR = ",";

	private File albumFile;

	/**
	 * Instantiates a new music library file reader.
	 *
	 * @precondition albumFile != null
	 * @postcondition none
	 * 
	 * @param albumFile
	 *            the album file
	 */
	public MusicLibraryFileReader(File albumFile) {
		if (albumFile == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.ALBUM_FILE_CANNOT_BE_NULL);
		}

		this.albumFile = albumFile;
	}

	/**
	 * Opens the music library file and reads the first field of the first line
	 * which is the album name and returns that line. If the file is empty, then an
	 * empty string is returned.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @throws FileNotFoundException
	 *             If file not found.
	 * 
	 * @return Album name in the file or empty string if no data in file.
	 */
	public String getAlbumName() throws FileNotFoundException {
		Scanner input = new Scanner(this.albumFile);
		input.useDelimiter(",");
		String albumName = input.next();
		input.close();

		return albumName;
	}

	private String[] parseAlbumLine(String line) {
		String[] fields = line.split(",");
		return fields;
	}

	/**
	 * Opens the associated mlf file and reads all the songs stored in the file one
	 * line at a time. Parses each line and creates a song object and stores it in
	 * an ArrayList of Song objects. Once the file has been completely read the
	 * ArrayList of Songs is returned from the method. Note: Currently, assumes all
	 * songs are from the same album.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return Collection of Song objects read from the file.
	 */
	public ArrayList<Song> loadAllMySongs() {
		ArrayList<Song> songs = new ArrayList<Song>();

		try (Scanner input = new Scanner(this.albumFile)) {
			while (input.hasNextLine()) {
				String line = input.nextLine();
				String[] fields = this.parseAlbumLine(line);

				if (fields.length != 6) {
					continue;
				}
				
				String title = fields[1];
				String artist = fields[2];
				int year = Integer.parseInt(fields[3]);
				double price = Double.parseDouble(fields[4]);
				int rating = Integer.parseInt(fields[5]);
				
				Song song = new Song(title, artist, year, price, rating);
				songs.add(song);
				
			}
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return songs;
	}
	
	/**
	 * Opens the associated mlf file and reads all the songs stored in the file one
	 * line at a time. Parses each line and creates a song object and stores it in
	 * proper albums within the library.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param library
	 * 			the library to modify
	 * 
	 */
	public void loadAllMySongs(Library library) {
		try (Scanner input = new Scanner(this.albumFile)) {
			while (input.hasNextLine()) {
				String line = input.nextLine();
				String[] fields = this.parseAlbumLine(line);
				if (fields.length != 6) {
					continue;
				}
				String album = fields[0];
				String title = fields[1];
				String artist = fields[2];
				int year = Integer.parseInt(fields[3]);
				double price = Double.parseDouble(fields[4]);
				int rating = Integer.parseInt(fields[5]);
				Song song = new Song(title, artist, year, price, rating);
				for (Album currAlbum : library.getAlbums()) {
					if (currAlbum.getName().equalsIgnoreCase(album)) {
						library.addSong(song, currAlbum);
						library.addSong(song, LibraryController.MY_SONGS_ALBUM);
						break;
					} else {
						Album newAlbum = new Album(album);
						library.addAlbum(newAlbum);
						library.addSong(song, album);
						library.addSong(song, LibraryController.MY_SONGS_ALBUM);
						break;
					}
				}
			}
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage());
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
	}
	
}