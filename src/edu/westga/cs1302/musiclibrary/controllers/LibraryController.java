package edu.westga.cs1302.musiclibrary.controllers;

import java.util.ArrayList;

import edu.westga.cs1302.musiclibrary.model.Album;
import edu.westga.cs1302.musiclibrary.model.Library;
import edu.westga.cs1302.musiclibrary.model.Song;
import edu.westga.cs1302.musiclibrary.resources.UI;

/**
 * Manages a library of albums with music. When created there will be a master
 * MY SONGS album which every song created will be a part of and then
 * additional albums can be created and songs added to these albums. A song can
 * be a part of multiple albums.
 * 
 * @author CS 1302
 * @version Spring 2018
 * 
 */
public class LibraryController {
	public static final String MY_SONGS_ALBUM = "MY SONGS";

	private Library musicLibrary;

	/**
	 * Creates the music library with a default MY SONGS album which every song
	 * created will be a part of.
	 * 
	 * @precondition none
	 * @postcondition numberAlbums() == 1
	 */
	public LibraryController() {
		this.musicLibrary = new Library();

		Album defaultAlbum = new Album(MY_SONGS_ALBUM);
		this.musicLibrary.addAlbum(defaultAlbum);
	}

	/**
	 * Adds a song to the library within the MY SONGS album.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param song
	 *            The song object to add to the album
	 * 
	 * @return true iff song was added to the album, false otherwise
	 */
	public boolean addSongToAllAlbums(Song song) {
		return this.musicLibrary.addSong(song, MY_SONGS_ALBUM);
	}

	/**
	 * Adds specified song to the specified album.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param song
	 *            Song to add to album
	 * @param album
	 *            Album to add song to
	 * @return Whether song was added to the album.
	 */
	public boolean addSong(Song song, Album album) {
		for (Song currSong : album.getSongs()) {
			if (currSong.getTitle().equalsIgnoreCase(song.getTitle()) && currSong.getArtist().equalsIgnoreCase(song.getArtist())) {
				return false;
			}
		}
		return this.musicLibrary.addSong(song, album);
	}

	/**
	 * Adds a song to the music library and to the MY SONGS album.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param title
	 *            The song's title
	 * @param artist
	 *            The song's artist
	 * @param year
	 *            The song's year
	 * @param price
	 *            The song's price
	 * @param rating
	 *            The song's rating
	 * 
	 * @return true iff song was added to the album, false otherwise
	 */
	public boolean addSong(String title, String artist, int year, double price, int rating) {
		Song song = new Song(title, artist, year, price, rating);

		return this.musicLibrary.addSong(song, MY_SONGS_ALBUM);
	}

	/**
	 * Updates specified song by title and artist with year and price.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param title
	 *            The song's title
	 * @param artist
	 *            The song's artist
	 * @param year
	 *            The song's year
	 * @param price
	 *            The song's price
	 * @param rating
	 *            The song's rating
	 * 
	 * @return true iff song was updated, false otherwise
	 */
	public boolean updateSong(String title, String artist, int year, double price, int rating) {
		Album album = this.musicLibrary.getAlbum(MY_SONGS_ALBUM);
		return album.updateSong(title, artist, year, price, rating);
	}

	/**
	 * Deletes the specified song (title and artist) from every album it is on.
	 *
	 * @precondition none
	 * @postcondition none
	 *
	 * @param title
	 *            The song's title
	 * @param artist
	 *            The song's artist
	 * 
	 * @return True iff finds and delete the song with specified title and artist,
	 *         false if song was not found
	 */
	public boolean deleteSong(String title, String artist) {
		boolean deletedOnOne = false;
		for (Album currAlbum : this.musicLibrary.getAlbums()) {
			boolean deleted = currAlbum.deleteSong(title, artist);

			if (deleted) {
				deletedOnOne = true;
			}
		}

		return deletedOnOne;
	}

	/**
	 * Adds specified song to specified album
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param song
	 *            Song to add
	 * @param albumName
	 *            Name of the album to add the song to.
	 * @return true, iff song was added to the specified album.
	 */
	public boolean addSong(Song song, String albumName) {
		return this.musicLibrary.addSong(song, albumName);
	}

	/**
	 * Adds specified song to the MY_SONGS album.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param song
	 *            Song to add
	 * 
	 * @return true, iff song was added to the ALL_SONGS album
	 */
	public boolean addSongToAllSongs(Song song) {
		return this.musicLibrary.addSong(song, MY_SONGS_ALBUM);
	}

	/**
	 * Creates the album
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param albumName
	 *            Album name to create
	 * @return true, iff new album with specified name created
	 */
	public boolean createAlbum(String albumName) {
		Album album = new Album(albumName);

		return this.musicLibrary.addAlbum(album);
	}

	/**
	 * Gets all the albums in the library.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return All the albums in the library.
	 */
	public ArrayList<Album> getAlbums() {
		return this.musicLibrary.getAlbums();
	}

	/**
	 * Gets all the songs that are part of the music library.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return All the songs that are part of the music library.
	 */
	public ArrayList<Song> getAllSongs() {
		return this.musicLibrary.getSongsOnAlbum(MY_SONGS_ALBUM);
	}

	/**
	 * Gets the album by name of the album.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param albumName
	 *            The album name to get
	 *
	 * @return the album with specified name
	 */
	public Album getAlbum(String albumName) {
		return this.musicLibrary.getAlbum(albumName);
	}

	/**
	 * Gets the songs on the specified album.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param albumName
	 *            Name of album to get all the songs from
	 * 
	 * @return the songs
	 */
	public ArrayList<Song> getSongsOnAlbum(String albumName) {
		Album album = this.musicLibrary.getAlbum(albumName);
		if (album != null) {
			return album.getSongs();
		}

		return null;
	}

	/**
	 * Adds the collection of songs to MY SONGS album.
	 * 
	 * @precondition songs != null
	 * @postcondition getSongsOnAlbum("MY SONGS").size() == 
	 *                  getSongsOnAlbum("MY SONGS").size()@prev + songs.size()
	 *
	 * @param songs
	 *            the songs to add to the album
	 */
	public void addSongsToMySongsAlbum(ArrayList<Song> songs) {
		if (songs == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.SONGS_CANNOT_BE_NULL);
		}

		Album album = this.musicLibrary.getAlbum(MY_SONGS_ALBUM);

		for (Song currSong : songs) {
			album.add(currSong);
		}

	}

	/**
	 * Adds the collection of songs to specified album and the ALL SONGS album.
	 * 
	 * @precondition songs != null
	 * @postcondition getSongsOnAlbum("MY SONGS").size() == 
	 *                  getSongsOnAlbum("MY SONGS").size()@prev + songs.size() AND
	 *                getSongsOnAlbum(albumName).size() ==
	 *                getSongsOnAlbum(album).size()@prev + songs.size()
	 * @param songs
	 *            the songs to add to the album
	 * @param albumName
	 *            the name of the album to add the songs to
	 * 
	 */
	public void addSongs(ArrayList<Song> songs, String albumName) {
		if (songs == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.SONGS_CANNOT_BE_NULL);
		}

		if (albumName == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.ALBUMNAME_CANNOT_BE_NULL);
		}

		if (albumName.isEmpty()) {
			throw new IllegalArgumentException(UI.ExceptionMessages.ALBUMNAME_CANNOT_BE_EMPTY);
		}

		this.addSongsToMySongsAlbum(songs);

		Album album = this.musicLibrary.getAlbum(albumName);

		for (Song currSong : songs) {
			album.add(currSong);
		}
	}

	/**
	 * Gets the library
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the music library
	 */
	public Library getLibrary() {
		return this.musicLibrary;
	}

}
